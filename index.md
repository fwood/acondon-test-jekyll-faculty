---
layout: default
---

## About Me

<img class="profile-picture" src="headshot.png">

I am a professor of computer science at the University of British Columbia.

  - Ph.D., University of Washington, 1987.
  - B.Sc., University College Cork, Ireland, 1982.


## Research Interests

I am interested in computational complexity theory and design of algorithms, and their applications in bioinformatics, biomolecular computation, hardware verification, and combinatorial auctions. Much of my current work focuses on prediction of the secondary structure of nucleic acids from the base sequence, informed by thermodynamic energy models, as well as applications of prediction tools to design of biomolecules.

For more background on our RNA work, see the nice  article in SIAM News [Sweet Structural Mysteries of Life](http://www.siam.org/pdf/news/1455.pdf), by Barry Cipra (2008). Dick Lipton's blog, Godel's Lost Letter, has an entry (2009) titled [The Unbounded Power of Randomness]( http://rjlipton.wordpress.com/2009/03/19/the-unbounded-power-of-randomness/)
which describes some of our joint theoretical work on the power of randomness and nondeterminism in finite state machines.
