---
title: Papers
layout: default
permalink: /papers/
---

# Google Scholar
[Recent First](https://scholar.google.com/citations?hl=en&user=SO_j4zwAAAAJ&view_op=list_works&sortby=pubdate), [Most Cited First](https://scholar.google.com/citations?user=SO_j4zwAAAAJ&hl=en)

{% bibliography --file Condon_Anne %}
