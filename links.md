---
title: Links
layout: default
permalink: /links/
---

  - [Information on Graduate admissions at UBC](http://www.cs.ubc.ca/prospective/grad/grad_welcome.shtml) You'll also find information about funding and great scholarship opportunities.
  - [The International Meeting on DNA Computing and Molecular Programming](http://www.dna-computing.org/)
  - [The BETA (Bioinformatics, and Empirical & Theoretical Algorithmics) Laboratory UBC](http://www.cs.ubc.ca/labs/beta/)
  - [Computing Research Association's Committee on the Status of Women in Computing Research](http://www.cra-w.org/)
  - [The Jade Project](jadeproject/index.html) supported by the NSERC/General Motors Canada Women in Science and Engineering for British Columbia and the Yukon
